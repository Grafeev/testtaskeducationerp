﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTaskEducationERP.Models
{
    public class NewMovieVm
    {
        public string Name { get; set; }
        public string Desc { get; set; }
        public string Producer { get; set; }
        public int? Year { get; set; }
    }
}

﻿using System;

namespace TestTaskEducationERP.Data
{
    public partial class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public int? Year { get; set; }
        public string Producer { get; set; }
        public string User { get; set; }
        public string PosterUrl { get; set; }
    }
}
﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using TestTaskEducationERP.Data;
using TestTaskEducationERP.Models;

namespace TestTaskEducationERP.Controllers
{
    public class MovieController : Controller
    {
        private readonly ApplicationDbContext _repository;
        private readonly IWebHostEnvironment _appEnvironment;
        public MovieController(ApplicationDbContext context, IWebHostEnvironment appEnvironment)
        {
            _repository = context;
            _appEnvironment = appEnvironment;
        }

        [HttpGet]
        public IActionResult Index(int id)
        {
            var model = _repository.AspNetMoviesTest.FirstOrDefault(a => a.Id == id);
            return View(model);
        }

        [Authorize]
        public IActionResult AddMovie()
        {
            return View();
        }

        [Authorize]
        public IActionResult EditMovie(int id)
        {
            var model = _repository.AspNetMoviesTest.FirstOrDefault(a => a.Id == id);
            return View(model);
        }

        [HttpGet]
        public JsonResult GetMovies(int page)
        {
            int count = 10;

            var list = _repository.AspNetMoviesTest.Skip(count * (page - 1)).Take(count)
                .Select(a => new { Id = a.Id, Name = a.Name, Year = a.Year }).ToList();
            
            var pages = Math.Ceiling((_repository.AspNetMoviesTest.Count() / 10.0));

            return Json(new { 
                List = list,
                Pages = pages
            });
        }

        [Authorize]
        [HttpPost]
        public async Task<JsonResult> AddMovie([FromBody]NewMovieVm model)
        {
            var movie = new Movie();
            movie.Name = model.Name;
            movie.Desc = model.Desc;
            movie.Producer = model.Producer;
            movie.Year = model.Year;
            movie.User = User.Identity.Name;

            _repository.AspNetMoviesTest.Add(movie);
            await _repository.SaveChangesAsync();

            string url = "/Movie/EditMovie/" + movie.Id;
            return Json(url);

        }

        [Authorize]
        [HttpPost]
        public async Task<JsonResult> SaveMovie([FromBody]Movie model)
        {
            _repository.AspNetMoviesTest.Update(model);
            await _repository.SaveChangesAsync();

            string url = "/Movie/Index/" + model.Id;
            return Json(url);
        }

        [HttpPost]
        public async Task<JsonResult> AddFile(IFormCollection uploadedFile)
        {
            string error = null;
            try
            {
                if (uploadedFile != null)
                {
                    var MovieId = JsonSerializer.Deserialize<int>(uploadedFile.First().Value);
                    var File = uploadedFile.Files.First();
                    string extension = Path.GetExtension(File.FileName);

                    string path = "/Files/" + Guid.NewGuid() + extension;
                    
                    using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                    {
                        await uploadedFile.Files.First().CopyToAsync(fileStream);
                    }
                    var movie = _repository.AspNetMoviesTest.FirstOrDefault(a => a.Id == MovieId);
                    movie.PosterUrl = path;
                    _repository.AspNetMoviesTest.Update(movie);
                    await _repository.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                error = e.Message;
            }

            return Json(error);
        }

    }
}
